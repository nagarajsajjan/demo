AWSTemplateFormatVersion: '2010-09-09'
Resources:
  MyLambdaFunction:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: S3NotificationConfigurator
      Handler: index.handler
      Role: !GetAtt LambdaExecutionRole.Arn
      Code:
        ZipFile: |
          import json
          import boto3
          import cfnresponse
          import logging

          s3 = boto3.client('s3')
          logger = logging.getLogger()
          logger.setLevel(logging.INFO)

          def handler(event, context):
              logger.info('Received event: %s', json.dumps(event))
              try:
                  if event['RequestType'] in ['Create', 'Update']:
                      bucket_name = event['ResourceProperties']['BucketName']
                      function_arn = event['ResourceProperties']['FunctionArn']
                      prefix = event['ResourceProperties']['Prefix']
                      suffixes = event['ResourceProperties']['Suffixes']

                      lambda_configurations = []
                      for suffix in suffixes:
                          lambda_configurations.append({
                              'LambdaFunctionArn': function_arn,
                              'Events': ['s3:ObjectCreated:*'],
                              'Filter': {
                                  'Key': {
                                      'FilterRules': [
                                          {'Name': 'prefix', 'Value': prefix},
                                          {'Name': 'suffix', 'Value': suffix}
                                      ]
                                  }
                              }
                          })

                      response = s3.put_bucket_notification_configuration(
                          Bucket=bucket_name,
                          NotificationConfiguration={
                              'LambdaFunctionConfigurations': lambda_configurations
                          }
                      )

                      logger.info('PutBucketNotificationConfiguration response: %s', response)
                  cfnresponse.send(event, context, cfnresponse.SUCCESS, {})
              except Exception as e:
                  logger.error('Error: %s', e)
                  cfnresponse.send(event, context, cfnresponse.FAILED, {})

      Runtime: python3.8
      Timeout: 60

  LambdaExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service: lambda.amazonaws.com
            Action: sts:AssumeRole
      Policies:
        - PolicyName: LambdaS3Policy
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                Resource: arn:aws:logs:*:*:*
              - Effect: Allow
                Action:
                  - s3:PutBucketNotificationConfiguration
                  - s3:GetBucketNotification
                Resource: arn:aws:s3:::YOUR_EXISTING_BUCKET_NAME  # Replace with your bucket ARN
              - Effect: Allow
                Action: lambda:InvokeFunction
                Resource: '*'

  BucketPolicy:
    Type: AWS::S3::BucketPolicy
    Properties:
      Bucket: YOUR_EXISTING_BUCKET_NAME  # Replace with your bucket name
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              AWS: !GetAtt LambdaExecutionRole.Arn
            Action:
              - s3:PutBucketNotificationConfiguration
              - s3:GetBucketNotification
            Resource: arn:aws:s3:::YOUR_EXISTING_BUCKET_NAME

  S3NotificationConfigurator:
    Type: Custom::S3NotificationConfigurator
    Properties:
      ServiceToken: !GetAtt MyLambdaFunction.Arn
      BucketName: YOUR_EXISTING_BUCKET_NAME  # Replace with your existing bucket name
      FunctionArn: !GetAtt MyLambdaFunction.Arn  # Replace with the ARN of your Lambda function
      Prefix: your/prefix/path/  # Replace with your desired prefix
      Suffixes:  # Define multiple suffixes
        - .csv
        - .txt

Outputs:
  LambdaFunctionArn:
    Description: "The ARN of the Lambda function"
    Value: !GetAtt MyLambdaFunction.Arn
